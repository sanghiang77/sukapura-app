import React from 'react';
import {Text, View} from 'react-native';

const IconWithText = (props) => {
  return (
    <View>
      <View style={{width: 50, height: 50, backgroundColor:'purple',borderRadius:30}}/>
  <Text style={{maxWidth:60, textAlign:'center', marginTop: 10}}>{props.title}</Text>
    </View>
  );
};

const IconAction = () => {
  return (
    <View style={{width: 35, height: 35, backgroundColor: '#EAEAEA', borderRadius: 35}} />
     
  );
};

const ScanQode = () => {
  return (
    <View style={{flex: 1}}>
      <View style={{flex: 1, backgroundColor:'grey'}}>
        <View style={{flexDirection:'row', paddingHorizontal: 16, justifyContent: 'space-between', marginTop: 16}}>
          <IconAction />
          <View style={{flexDirection: 'row', justifyContent:'space-between', width:80,}}>
            <IconAction />
            <IconAction />
          </View>
        </View>
      </View>
      <View style={{height: 200, backgroundColor: '#FFFF', paddingHorizontal:16}}>
        <View style={{alignItems: 'center', marginTop: 8, marginBottom: 18}}>
            <View style={{width:35, height:2, backgroundColor:'#EAEAEA', marginVertical:2}}/>
            <View style={{width:35, height:2, backgroundColor:'#EAEAEA', marginVertical:2}}/>
        </View>
        <View style={{flexDirection:'row', justifyContent:'space-between',alignItems:'center'}}>
          <Text style={{fontSize:24, fontWeight:'bold', color:'black'}}>Pilihan Lainnya</Text>
          <Text style={{fontSize:14, fontWeight:'bold', color:'purple'}}>Shortcut</Text>
        </View>
        <View style={{flexDirection:'row', alignItems:'flex-start', marginTop:14, width:'100%'}}>
          <View style={{flexDirection:'row', alignItems:'flex-start', width: 160, justifyContent:'space-between', paddingRight: 12}}>
            <IconWithText title="Nomer Hape"/>
            <IconWithText title="ScanQR"/>
          </View>
          <View style={{ width:1, height:60, backgroundColor:'#EAEAEA'}}/>
          <View style={{flex:1, paddingLeft: 12}}>
          <Text>
            Angge iYeuWe , Kangge Bayar Naon Wae Di Dieu. Haratis Teu Angge
            Admin!!
          </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default ScanQode;
